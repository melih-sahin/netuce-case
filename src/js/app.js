Number.prototype.formatMoney = function (fractionDigits, decimal, separator) {
  fractionDigits = isNaN((fractionDigits = Math.abs(fractionDigits)))
    ? 2
    : fractionDigits;

  decimal = typeof decimal === "undefined" ? "." : decimal;

  separator = typeof separator === "undefined" ? "," : separator;

  let number = this;

  let neg = number < 0 ? "-" : "";

  let wholePart =
    parseInt((number = Math.abs(+number || 0).toFixed(fractionDigits))) + "";

  var separtorIndex =
    (separtorIndex = wholePart.length) > 3 ? separtorIndex % 3 : 0;

  return (
    neg +
    (separtorIndex ? wholePart.substr(0, separtorIndex) + separator : "") +
    wholePart
      .substr(separtorIndex)
      .replace(/(\d{3})(?=\d)/g, "$1" + separator) +
    (fractionDigits
      ? decimal +
        Math.abs(number - wholePart)
          .toFixed(fractionDigits)
          .slice(2)
      : "")
  );
};

function formatMoney(raw) {
  return Number(raw).formatMoney(2, ",", ".");
}

function moneyConvertText(num) {
  numValue = num
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    .split(".");
  intValue = numValue[0];
  decimalValue = numValue[1] || "";

  let ones = [
    "",
    "bir",
    "iki",
    "üç",
    "dört",
    "beş",
    "altı",
    "yedi",
    "sekiz",
    "dokuz",
  ];
  let tens = [
    "",
    "on",
    "yirmi",
    "otuz",
    "kırk",
    "elli",
    "altmış",
    "yetmiş",
    "seksen",
    "doksan",
  ];
  let thousands = [
    "",
    "bin",
    "milyon",
    "milyar",
    "trilyon",
    "katrilyon",
    "kentrilyon",
  ];
  let result = [];

  let step = 0;
  for (i = intValue.split(",").length; i > 0; i--) {
    num = intValue.split(",")[i - 1];
    if (num.length == 1) {
      num = "00" + num;
    }
    if (num.length == 2) {
      num = "0" + num;
    }
    c = "";

    for (j = 1; j < num.length + 1; j++) {
      if (j == 1 && num[j - 1] == 1) {
        c += " yüz ";
      } else if (j == 1 && ones[num[j - 1]] != "") {
        c += ones[num[j - 1]] + " yüz ";
      } else if (j == 2) {
        c += tens[num[j - 1]] + " ";
      } else if (
        j == 3 &&
        intValue.length == 5 &&
        num[j - 1] == 1 &&
        step == 1
      ) {
        c += " ";
      } else if (j == 3) {
        c += ones[num[j - 1]] + " ";
      }
    }

    if (c != "") {
      result.push(c + thousands[step]);
    }
    step++;
  }

  if (result.length != 0) {
    result = result.reverse().join(" ") + " TL";
  } else {
    result = "";
  }
  if (decimalValue.length == 1) {
    decimalValue = decimalValue + "0";
  }
  if (decimalValue != "") {
    result += " " + tens[decimalValue[0]] + " " + ones[decimalValue[1]] + " kuruş";
  }

  result = result.replace(/ /g, " ").trim();

  return result;
}

let dataSourceArray = {
  "": [
    {
      productid: "LT-1449506",
      productName: "Super Outfit",
      productPrice: 300.45,
      qnty: 10,
      price: 9000,
    },
    {
      productid: "LT-4523632",
      productName: "Rocket-Powered Roller Skates",
      productPrice: 1136799,
      qnty: 4,
      price: 1200,
    },
    {
      productid: "TM-2026045",
      productName: "Giant Kite Kit",
      productPrice: 1099.9,
      qnty: 12,
      price: 1500,
    },
    {
      productid: "LT-1990790",
      productName: "Bird Seed",
      productPrice: 5.9,
      qnty: 55,
      price: 9700,
    },
    {
      productid: "TLTS-8002604",
      productName: "Artificial Rock",
      productPrice: 123.99,
      qnty: 24,
      price: 1500,
    },
    {
      productid: "TM-6792415",
      productName: "Giant Rubber Band V1",
      productPrice: 44.9,
      qnty: 5000,
      price: 1300.23,
    },
    {
      productid: "LT-8163469",
      productName: "Jet Motor",
      productPrice: 99999.99,
      qnty: 2,
      price: 1000,
    },
  ],
};

let tableHeadersArray = [
  "Ürün kodu",
  "Ürün adı",
  "Birim fiyat(TL)",
  "Adet",
  "Fiyat(TL)",
];

let ntcTable = new NTCTable(dataSourceArray, tableHeadersArray);
ntcTable.withIndicatorArea = false;

$("#ntcTable").html(ntcTable.generateInnerHtmlCode());

let prices = $(".ntc-tableRow-regular > td:last-child");
let sum = 0;

$("table:first tr").each(function (index) {
  if (index > 0) {
    let price = parseFloat($(this).find(":last-child").text());
    sum += price;
  }
});

$("#result").html(
  `<h1 class="resultText">${sum.formatMoney(2, ",", ".")} TL</h1>`
);
$("#money-text").html(`<h6>Yalnız ${moneyConvertText(sum)}</h6>`);
